#!/usr/bin/env bash

# Files:
## .vimrc
## .vimrc.local
## .vim/templates/skeleton.proj.sh

CHROOT="`dirname ${BASH_SOURCE[0]}`/chroot"

warning()
{
    counter=0
    echo "WARNING: This action will override old settings in $1. Do you want to continue?"
    ans=""
    while [[ -z "$ans" ]]; do
        echo -n "Do you want to continue? [yes/No]: "
        read ans
        ans="${ans:0:1}"
        if [[ ${ans,,} == "y" ]]; then
            return 0
        elif [[ ${ans,,} == "n" ]]; then
            return 1
        fi
        ans=""
        [[ counter -eq 2 ]] && return 1
        counter=$((counter + 1))
    done
    return 1
}

install ()
{
    ## .vimrc ##
    [[ -c ~/.vimrc ]] || warning "~/.vimrc" || exit 0
    echo "Installing '~/.vimrc' ..."
    curl 'https://vim-bootstrap.com/generate.vim' --data 'editor=vim' > ~/.vimrc || exit 1

    ## .vimrc.local ##
    [[ -c ~/.vimrc.local ]] || warning "~/.vimrc.local" || exit 0
    echo "Installing '~/.vimrc.local' ..."
    cp $CHROOT/.vimrc.local ~/.vimrc.local || exit 1

    ## .vim/templates/skeleton.proj.sh
    echo "Installing '~/.vim/templates/skeleton.proj.sh' ..."
    mkdir -p ~/.vim/templates/
    cp $CHROOT/.vim/templates/skeleton.proj.sh ~/.vim/templates/skeleton.proj.sh || exit 1

    echo "Done"
    exit 0
}

uninstall ()
{
    echo "Uninstalling '~/.vimrc' ..."
    echo "Uninstalling '~/.vimrc.local' ..."
    echo "Uninstalling '~/.vim/templates/skeleton.proj.sh' ..."
    rm ~/.vimrc ~/.vimrc.local ~/.vim/templates/skeleton.proj.sh
    rmdir --ignore-fail-on-non-empty ~/.vim/templates
    echo "Done"
}

help ()
{
    echo "./INSTALL.sh [install|uninstall]"
}

case ${1,,} in
    install)
        install
        ;;
    uninstall)
        uninstall
        ;;
    *)
        help
        ;;
esac


