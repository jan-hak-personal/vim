#!/usr/bin/env bash

#This script just receive two variables
## 1) Target    - name of target
## 2) Action    - action is enumeration of 2 constants { Release, Debug }

[[ $# -le 1 ]] && echo "ERROR: Not enough arguments" && exit 1

case $1 in
    "<++>")
        case $2 in
            "Release")
                <++>
                ;;
            "Debug")
                <++>
                ;;
            *)
                echo "ERROR: Unknown Action"
                exit 1
                ;;
        esac
        ;;
    "--list")
        # here should be listed all Targets
        echo "<++>\t<++>"
        ;;
    "*")
        echo "ERROR: Unknown Target"
        exit 1
        ;;
esac

exit 0
